function [ idx ] = date_interval( calendar, date1, date2 )
%DATE_INTERVAL Summary of this function goes here
%   Detailed explanation goes here

idx = after_date(calendar, date1) & before_date(calendar, date2);

end

