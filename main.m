
% loading data
[naps_data, calendar]   = load_naps();
[subject_id, move_in_date, lat, long, no2] = load_child();
center_gps  = load_center_gps();
doc_dob     = load_doc_dob();

% number of subjects
n_subjects = length(subject_id);

% finding the closest center to each address based on GPS coordinates
closest_center = find_closest_center(center_gps, lat, long);

% opening the result file for writing
file_id = fopen('child_adjusted_no2.txt', 'w');

% for each subject
for i = 1:n_subjects
    
    % getting the conception dates
    conception_dates = get_conception_dates(subject_id(i), doc_dob, calendar);
    
    % count the number of days between conception and birth
    n_days = size(conception_dates, 1);
    
    % getting for every day between begining and end of conception, at
    % which address where the people living
    address_id = get_address_id(move_in_date(i, :), conception_dates);
    
    % initialize an array in which we will store the daily no2
    no2_history = zeros(n_days, 1);
    
    % doing a loop over the days from conception to birth
    for j = 1:n_days
        
        % getting address id for this given day
        k = address_id(j)-1;
        
        % if first address is after doc, k==0 and set no2 to -9999
        if k == 0
            no2_today = -9999;
        else
            % otherwise...
            
            % getting geospatial no2 information from master file data
            no2_here = no2(i, k);
            
            % finding the closest center to address
            center = closest_center{i, k};
            
            % getting the yearly no2 for this center for LUR model
            center_idx = find(strcmp(center_gps{1}, center));
            year_no2 = center_gps{4}(center_idx);
            
            % obtaining the no2 for the gicen center and given date
            daily_no2 = get_daily_no2(center, naps_data, conception_dates(j, :));
            
            % il manque le 31 javnvier et le 31 mars 2012 a winnipeg.
            % Remplacer par MISSING
            if isempty(daily_no2)
                daily_no2 = -9999;
            end
            
            % if the address does not have gps coordinates make it Missing
            if strcmp(center, 'None')
                no2_today = -9999;
            else
                % otherwise computing the correction
                no2_today = no2_here * daily_no2 / year_no2;
            end
            
        end
        
        if (no2_today < 0)
            % if the data is missing, disregard the correction
            no2_today = -9999;
        end
        
        no2_history(j) = no2_today;
    end
    
    print_result_row(file_id, subject_id(i), no2_history);
    
end

% closing the result file
fclose(file_id);

