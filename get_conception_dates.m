function conception_dates = get_conception_dates( subject_id, doc_dob, calendar )
% getting an array filled with calendar entries corresponding to the
% conception period

% get the begining and end of conception dates in numerical form
% finding the subject in the doc dob file
idx = find(doc_dob(:, 1) == subject_id);

% storing the dates
doc = doc_dob(idx, 2:4);
dob = doc_dob(idx, 5:7);

% find days that are between conception and birth
conception_idx = date_interval(calendar, doc, dob);
conception_dates = calendar(conception_idx, :);


end

