function [ res ] = after_date( calendar, date )
%DATE_COMPARE Summary of this function goes here
%   Detailed explanation goes here

res = (calendar(:, 1) > date(1)) | ...
    (calendar(:, 1) == date(1) & calendar(:, 2) > date(2)) | ...
    (calendar(:, 1) == date(1) & calendar(:, 2) == date(2) & calendar(:, 3) >= date(3));

end

