function [ daily_no2 ] = get_daily_no2( center, naps_data, date )
% get the daily no2 for the given center at that date

% finding the row in the naps data with the given center and date
naps_data_idx = strcmp(naps_data{1}, center) & ...
    naps_data{4} == date(1) & ...
    naps_data{3} == date(2) & ...
    naps_data{2} == date(3);

% returning the corresponding no2 for that day
daily_no2 = naps_data{7}(naps_data_idx);

% if december 2009 in Winnipeg, use monthly no2 instead of daily (missing data)
if (strcmp(center, 'Winnipeg') && isequal(date(1:2), [2009, 12]))
    daily_no2 = naps_data{8}(naps_data_idx);
end

end

