function [ res ] = load_doc_dob( )

% opening the file
fid = fopen('data/txt/ID_DOB_DOC.txt', 'r');

% format of the data in the text file
format_string = '%s %s %f %s %s'; % string string float string string

% read the data into scan
scan = textscan(fid, format_string, ...
                    'Delimiter', '\t', 'HeaderLines', 1, ...
                    'TreatAsEmpty', {'None', '#N/A', 'not found'});
                
% close the file fid
fclose(fid);

% allocating the output
res = zeros(length(scan{1}), 7);

% filling the first column with subject id
res(:, 1) = scan{3};

% transforming the date string into integers
for i = 1:length(scan{1})
    res(i, 2:4) = sscanf(scan{5}{i}, '%f/%f/%f');
    res(i, 5:7) = sscanf(scan{4}{i}, '%f/%f/%f');
end

% shuffeling the columns to be in format [id year month day year month day]
res(:, 2:4) = res(:, [4, 2, 3]);
res(:, 5:7) = res(:, [7, 5, 6]);

end