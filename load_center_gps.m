function res = load_center_gps() 

fid = fopen('data/txt/center_gps.txt', 'r');
format_string = '%s %f %f %f';
res = textscan(fid, format_string, ...
                    'Delimiter', '\t', 'HeaderLines', 0);
fclose(fid);

end