function print_result_row(file_id, subject_id, no2_history)

% printing the subject number to the result file
fprintf(file_id, '%d', subject_id);

% write the no2_history
for i = 1:length(no2_history)
    fprintf(file_id, '\t%f', no2_history(i));
end

% write an end-of-line
fprintf(file_id, '\n');

end

