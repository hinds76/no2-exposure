function [ closest_center ] = find_closest_center( center_gps, lat, long )

n_subjects = length(lat);

% finding the closest center to each address based on GPS coordinates
closest_center = cell(n_subjects, 3);
for i = 1:3
    % computing the euclidean distance
    dx = bsxfun(@minus, lat(:, i), center_gps{2}');
    dy = bsxfun(@minus, long(:, i), center_gps{3}');
    d = sqrt(dx.^2 + dy.^2);
    
    % finding the closest one
    [~, idx] = min(d, [], 2);
    
    % storing the closest center
    for j = 1:n_subjects
        if ~isnan(d(j, idx(j)))
            closest_center{j, i} = center_gps{1}{idx(j)};
        else
            % if the gps coordinates are NaN, store None
            closest_center{j, i} = 'None';
        end
    end
end

end

